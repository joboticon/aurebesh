// Home.qml
//
// This file is part of the Aurebesh application.
//
// Copyright (c) 2017 
//
// Maintained by Joe (@exar_kun) <joe@ubports.com>
//
// GNU GENERAL PUBLIC LICENSE
//    Version 3, 29 June 2007
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.5
import Ubuntu.Components 1.3
import "modules"
import "../assets/trainer.js" as Testword

Page {
    id: homePage   
    title: i18n.tr("Aurebesh")
    header: DefaultHeader {}
    ScrollView {
        id: scroll
        anchors {
            fill: parent
            topMargin: homePage.header.height
        }

        Column {
            id: homeColumn
            width: scroll.width
            spacing: units.gu(2)



            Label {
                id: spacer
                text: " "
             }

         Button {
             anchors.horizontalCenter: parent.horizontalCenter
             text: (settings.testable == "words") ? i18n.tr("Switch to Letters") : i18n.tr("Switch to Words");
             onClicked: {
                 (settings.testable == "words") ? settings.testable = "letters" : settings.testable = "words";
                 theWord.text = (settings.testable == "words") ? wordList.words[Math.floor(Math.random() * wordList.words.length)] : letterList.chars[Math.floor(Math.random() * letterList.chars.length)]
                 answer.text = "";
             }
         }

	 DefaultLabel {
                 FontLoader {
                     id: abFont
                     source: "../assets/aurebesh.ttf"
                     }
                 id: theWord
                 text: (settings.testable == "words") ? wordList.words[Math.floor(Math.random() * wordList.words.length)] : letterList.chars[Math.floor(Math.random() * letterList.chars.length)]
                 font.pixelSize: 100
                 font.family: abFont.name
                 lineHeight: 1.5
                 color: "#00ccff"
             }


             TextField {
                 id: answer
                 text: ""
                 anchors.horizontalCenter: parent.horizontalCenter
                 focus: true
                 color: "#00ccff"
                 }


             DefaultLabel {
                 id: status
                 anchors.horizontalCenter: parent.horizontalCenter
                 text: i18n.tr("Do. Or do not.")
                 color: "#00ccff"
                 }

             DefaultLabel {
                 id: streak
                 anchors.horizontalCenter: parent.horizontalCenter
                 text: i18n.tr("Streak: " + settings.score)
                 color: "#00ccff"
                 }

             Item {
                 id: response
                 property var resp: [
                     i18n.tr("Impressive."),
                     i18n.tr("There is no try.")
                     ]
                 }


             Rectangle {
                 id: checkButton
                 height: checkText.height + 1
                 width: checkText.width + 1
                 Text { 
                     id: checkText
                     text: i18n.tr("Check") 
                     color: "#00ccff"
                     }
                 anchors.horizontalCenter: parent.horizontalCenter
                 MouseArea {
                     anchors.fill: parent
                     onPressed: checkAction
                     }
                 color: "#000000"
                 border.width: 1
                 border.color: "#00ccff"
                 }

             Button {
                 id: skipButton
                 text: i18n.tr("Skip")
                 anchors.horizontalCenter: parent.horizontalCenter
                 action: skipAction
                 }
               

         Action {
             id: checkAction
             onTriggered: { 
                 settings.score = (Testword.responder(answer.text, theWord.text) == 0) ? settings.score += 1 : 0
                 status.text = response.resp[Testword.responder(answer.text, theWord.text)]
                 theWord.text = Testword.checker(answer.text, theWord.text)
                 answer.text = ""
                 console.log("Score: " + settings.score)
                 }
             }

         Action {
             id: skipAction
             onTriggered: {
                 theWord.text = (settings.testable == "words") ? wordList.words[Math.floor(Math.random() * wordList.words.length)] : letterList.chars[Math.floor(Math.random() * letterList.chars.length)]
                 answer.text = ""
                 status.text = i18n.tr("Do. Or do not.")
                 settings.score = 0
                 }
             }

         LetterList { 
            id: letterList
         }

         WordList {
            id: wordList
         }

        }
    }
}
